﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Threading;
using GhostMainframe.Control.Input;
using GhostMainframe.Control.Input.Gamepad;

namespace GhostMainframe
{
    public class Core
    {
        protected static void controller_ButtonUpEvent(object sender, GamepadEventArgs e)
        {
            Console.WriteLine("Button up: {0}", e);
        }

        protected static void controller_ButtonDownEvent(object sender, GamepadEventArgs e)
        {
            Console.WriteLine("Button down: {0}", e);
        }

        protected static void controller_ThumbstickMove(object sender, GamepadEventArgs e)
        {
            Console.WriteLine("{0} move: {1}", e.Button, e);
        }

        public static void Main(string[] args)
        {
            Device gamepad = InputDevices.DetectGamepad();
            if (gamepad != null)
            {
                XboxWirelessController controller = new XboxWirelessController(gamepad);
                Console.WriteLine("Gamepad = {0}, Path = {1}", controller.Name, controller.EvdevPath);

                controller.ButtonDown += controller_ButtonDownEvent;
                controller.ButtonUp += controller_ButtonUpEvent;
                controller.ThumbstickMove += controller_ThumbstickMove;

                Thread t = new Thread(() =>
                {
                    using (EvdevReader reader = new EvdevReader(controller))
                    {
                        reader.Read();
                    }
                });

                t.Start();
                t.Join();
            }
            else
            {
                Console.WriteLine("No gamepad device found.");
            }
            Console.ReadLine();
        }
    }
}
