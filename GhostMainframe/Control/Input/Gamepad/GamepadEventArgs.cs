﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using GhostMainframe.Common;
using GhostMainframe.Control.Input.Gamepad;

namespace GhostMainframe.Control.Input
{
    public class GamepadEventArgs : EventArgs
    {
        public EventType Type { get; set; }

        public Button Button { get; set; }

        public InputEventCode InputEventCode { get; set; }

        public AbsoluteAxes AbsoluteAxis { get; set; }

        public int Value { get; set; }

        public GamepadEventArgs()
        {

        }

        public override string ToString()
        {
            return string.Format(
                "[GamepadEventArgs: Type={0}, Button={1}, InputEventCode={2}, AbsoluteAxis={3}, Value={4}]",
                Type, Button, InputEventCode, AbsoluteAxis, Value);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h#L35
        /// This is only a subset of event types supported by the Xbox Wireless Controller.
        /// </summary>
        public enum EventType
        {
            Syn = 0x00,
            Key = 0x01,
            Abs = 0x03,
            Msc = 0x04
        }

        /// <summary>
        /// https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h#L714
        /// This is only a subset of axes supported by the Xbox Wireless Conrtoller. 
        /// </summary>
        public enum AbsoluteAxes
        {
            AbsX        = 0x00, //  0
            AbsY        = 0x01, //  1
            AbsZ        = 0x02, //  2
            AbsRZ       = 0x05, //  5   
            AbsGas      = 0x09, //  9
            AbsBrake    = 0x0a, // 10
            AbsHat0X    = 0x10, // 16
            AbsHat0Y    = 0x11, // 17
            AbsMisc     = 0x28  // 40
        }
    }
}
