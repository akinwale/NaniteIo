﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System.Collections.Generic;
using GhostMainframe.Common; 

namespace GhostMainframe.Control.Input.Gamepad
{
    public class XboxWirelessController : GenericGamepad
    {
        public XboxWirelessController() : base()
        {
            
        }

        public XboxWirelessController(Device device) : base(device)
        {

        }

        /// <summary>
        /// Dispatches an EV_ABS event.
        /// </summary>
        /// <param name="code">the absolute axis code</param>
        /// <param name="value">the event value</param>
        public override void DispatchAbsEvent(short code, int value)
        {
            /*
             * Digital DPAD
             *   ABS_HAT0X      -1  Left
             *   ABS_HAT0X       1  Right
             *   ABS_HAT0Y      -1  Up
             *   ABS_HAT0Y       1  Down
             * 
             * Triggers
             *   ABS_BRAKE      Left trigger
             *   ABS_GAS        Right trigger
             * 
             * Thumbsticks
             *   ABS_X, ABS_Y   Left thumbstick
             *   ABS_Z, ABS_RZ  Right thumbstick
             */

            GamepadEventArgs.AbsoluteAxes axis = (GamepadEventArgs.AbsoluteAxes)code;
            GamepadEventArgs e = new GamepadEventArgs() { Type = GamepadEventArgs.EventType.Abs, AbsoluteAxis = axis, Value = value };
            switch (axis)
            {
                case GamepadEventArgs.AbsoluteAxes.AbsHat0X:
                    if (value == -1)
                        e.Button = Button.DpadLeft;
                    else if (value == 1)
                        e.Button = Button.DpadRight;

                    break;

                case GamepadEventArgs.AbsoluteAxes.AbsHat0Y:
                    if (value == -1)
                        e.Button = Button.DpadUp;
                    else if (value == 1)
                        e.Button = Button.DpadDown;

                    break;

                case GamepadEventArgs.AbsoluteAxes.AbsBrake:
                    e.Button = Button.LeftTrigger;
                    break;

                case GamepadEventArgs.AbsoluteAxes.AbsGas:
                    e.Button = Button.RightTrigger;
                    break;

                case GamepadEventArgs.AbsoluteAxes.AbsX:
                case GamepadEventArgs.AbsoluteAxes.AbsY:
                    e.Button = Button.LeftThumbstick;
                    break;

                case GamepadEventArgs.AbsoluteAxes.AbsZ:
                case GamepadEventArgs.AbsoluteAxes.AbsRZ:
                    e.Button = Button.RightThumbstick;
                    break;
            }

            if (axis == GamepadEventArgs.AbsoluteAxes.AbsHat0X ||
                axis == GamepadEventArgs.AbsoluteAxes.AbsHat0Y ||
                axis == GamepadEventArgs.AbsoluteAxes.AbsBrake ||
                axis == GamepadEventArgs.AbsoluteAxes.AbsGas)
            {
                // button down / up events should suffice for the dpad and triggers
                if (value == 0)
                    OnButtonUp(e);
                else
                    OnButtonDown(e);
            }
            else
            {
                OnThumbstickMove(e);
            }
        }

        protected override void InitialiseButtons()
        {
            Buttons = new HashSet<Button>()
            {
                Button.A,
                Button.B,
                Button.X,
                Button.Y,
                Button.Start,
                Button.Select,
                Button.Mode,
                Button.DpadLeft,
                Button.DpadUp,
                Button.DpadRight,
                Button.DpadDown,
                Button.LeftBumper,
                Button.LeftTrigger,
                Button.LeftThumbstick,
                Button.RightBumper,
                Button.RightTrigger,
                Button.RightThumbstick
            };
        }

        /// <summary>
        /// The Xbox Wireless Controller gets BtnNorth and BtnWest wrong, so we fix it here.
        /// </summary>
        protected override void InitialiseKeymap()
        {
            // RightTrigger, RightTrigger and Dpad* buttons are identified as EV_ABS,
            // so no direct event code button map will be definend here.
            Keymap = new Dictionary<InputEventCode, Button>()
            {
                { InputEventCode.BtnSouth,      Button.A },
                { InputEventCode.BtnEast,       Button.B },
                { InputEventCode.BtnNorth,      Button.X },
                { InputEventCode.BtnWest,       Button.Y },
                { InputEventCode.BtnStart,      Button.Start },
                { InputEventCode.KeyBack,       Button.Select },
                { InputEventCode.KeyHomepage,   Button.Mode },
                { InputEventCode.BtnTL,         Button.LeftBumper },
                { InputEventCode.BtnTR,         Button.RightBumper },
                { InputEventCode.BtnThumbL,     Button.LeftThumbstick },
                { InputEventCode.BtnThumbR,     Button.RightThumbstick }
            };
        }
    }
}
