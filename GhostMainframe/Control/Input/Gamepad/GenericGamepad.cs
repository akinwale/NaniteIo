﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System.Collections.Generic;
using GhostMainframe.Common;

namespace GhostMainframe.Control.Input.Gamepad
{
    public abstract class GenericGamepad : Device
    {
        public HashSet<Button> Buttons { get; protected set; }

        public Dictionary<InputEventCode, Button> Keymap { get; protected set; }

        protected abstract void InitialiseButtons();

        protected abstract void InitialiseKeymap();

        public GenericGamepad()
        {
            Buttons = new HashSet<Button>();
            Keymap = new Dictionary<InputEventCode, Button>();
            InitialiseButtons();
            InitialiseKeymap();
        }

        public GenericGamepad(Device device)
            : this()
        {
            Name = device.Name;
            EvdevPath = device.EvdevPath;
            IsGamepad = device.IsGamepad;
        }

        public bool IsButtonSupported(Button button)
        {
            return Buttons.Contains(button);
        }

        public bool IsInputEventKeyCodeSupported(InputEventCode inputEventCode)
        {
            return Keymap.ContainsKey(inputEventCode);
        }

        public void AddButton(Button button)
        {
            Buttons.Add(button);
        }

        public void AddKeymap(InputEventCode inputEventCode, Button button)
        {
            Keymap[inputEventCode] = button;
        }


        public Button this[InputEventCode eventCode]
        {
            get
            {
                if (!Keymap.ContainsKey(eventCode))
                {
                    throw new KeyNotFoundException(string.Format("The event code {0} is not supported.", eventCode));
                }

                return Keymap[eventCode];
            }
        }

        #region event dispatchers
        /// <summary>
        /// Dispatches an EV_ABS event.
        /// </summary>
        /// <param name="code">the absolute axis code</param>
        /// <param name="value">the event value</param>
        public abstract void DispatchAbsEvent(short code, int value);

        /// <summary>
        /// Dispatches an EV_KEY event. Every gamepad is expected to have buttons (keys),
        /// so this can be implemented here.
        /// </summary>
        /// <param name="code">the input event key code</param>
        /// <param name="value">the value, which is either 1 for down or 0 for up</param>
        public void DispatchKeyEvent(short code, int value)
        {
            InputEventCode keyCode = (InputEventCode) code;
            GamepadEventArgs e = new GamepadEventArgs()
            {
                Type = GamepadEventArgs.EventType.Key,
                Button = IsInputEventKeyCodeSupported(keyCode) ? this[keyCode] : Button.None,
                InputEventCode = keyCode,
                Value = value // not exactly necessary for button down / up events.
            };

            if (value == 0)
                OnButtonUp(e);
            else
                OnButtonDown(e);
        }
        #endregion

        public delegate void ButtonDownEventHandler(object sender, GamepadEventArgs e);

        public delegate void ButtonUpEventHandler(object sender, GamepadEventArgs e);

        public delegate void ThumbstickMoveEventHandler(object sender, GamepadEventArgs e);

        public event ButtonDownEventHandler ButtonDown;

        public event ButtonUpEventHandler ButtonUp;

        public event ThumbstickMoveEventHandler ThumbstickMove;

        public virtual void OnButtonDown(GamepadEventArgs e)
        {
            ButtonDownEventHandler handler = ButtonDown;
            if (handler != null)
            {
                ButtonDown(this, e);
            }
        }

        public virtual void OnButtonUp(GamepadEventArgs e)
        {
            ButtonUpEventHandler handler = ButtonUp;
            if (handler != null)
            {
                ButtonUp(this, e);
            }
        }

        public virtual void OnThumbstickMove(GamepadEventArgs e)
        {
            ThumbstickMoveEventHandler handler = ThumbstickMove;
            if (handler != null)
            {
                ThumbstickMove(this, e);
            }
        }
    }
}
