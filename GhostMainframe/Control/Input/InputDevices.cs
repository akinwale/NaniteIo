﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GhostMainframe.Common;

namespace GhostMainframe.Control.Input
{
    public class InputDevices
    {
        const string InputDevicesFile = "/proc/bus/input/devices";

        /// <summary>
        /// Detects the first gamepad device connected to the system.
        /// </summary>
        /// <returns>the first gamepad found in /proc/bus/input/devices, or null if no gamepad was found</returns>
        public static Device DetectGamepad()
        {
            Device gamepad = null;
            using (StreamReader reader = new StreamReader(new FileStream(
                InputDevicesFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                Device device = new Device();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    // Get the device name
                    if (line.StartsWith("N", StringComparison.InvariantCulture))
                    {
                        device.Name = line.Substring(line.IndexOf('"') + 1, line.Length - line.IndexOf('"') - 2);
                    }

                    // Get event* handler (for /dev/input/event*)
                    if (line.StartsWith("H", StringComparison.InvariantCulture)) // handlers
                    {
                        string[] handlers = line.Substring(line.IndexOf('=') + 1).Trim().Split(' ');

                        foreach (string handler in handlers)
                        {
                            if (handler.StartsWith("event", StringComparison.InvariantCulture))
                            {
                                device.EvdevPath = string.Format("/dev/input/{0}", handler);
                            }
                        }
                    }

                    // Find the KEY bitmap and parse (if present)
                    if (line.StartsWith("B", StringComparison.InvariantCulture) && line.Contains("KEY"))
                    {
                        StringBuilder sb = new StringBuilder();
                        string key = line.Substring(line.IndexOf('=') + 1);
                        string[] groups = key.Trim().Split(' ');
                        foreach (string grp in groups)
                        {
                            if (grp == "0")
                            {
                                sb.Append(string.Empty.PadLeft(64, '0'));
                            }
                            else
                            {
                                StringBuilder grpBin = new StringBuilder();
                                foreach (char c in grp)
                                {
                                    grpBin.Append(Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0'));
                                }
                                sb.Append(grpBin.ToString().PadLeft(64, '0'));
                            }
                        }

                        string bin = sb.ToString();
                        List<int> keybits = new List<int>();
                        // Count the bits with j starting from right to left
                        for (int i = bin.Length - 1, j = 0; i > -1; i--, j++)
                        {
                            if (bin[i] == '1')
                            {
                                keybits.Add(j);
                            }
                        }

                        // BtnSouth / BtnGamepad key indicates that the device is a gamepad
                        if (keybits.Contains((int) InputEventCode.BtnGamepad))
                        {
                            device.IsGamepad = true;
                            gamepad = device;
                            break;
                        }

                        // Next device
                        if (line.Trim().Length == 0)
                        {
                            device = new Device();
                        }
                    }
                }
            }

            return gamepad;
        }

        public InputDevices()
        {
            
        }
    }
}
