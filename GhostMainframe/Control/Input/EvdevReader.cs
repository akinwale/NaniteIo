﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.IO;
using GhostMainframe.Control.Input.Gamepad;

namespace GhostMainframe.Control.Input
{
    public class EvdevReader : IDisposable
    {
        private bool disposed;

        private bool open;

        private Device device;

        private FileStream stream;

        public EvdevReader(Device device)
        {
            this.device = device;
            stream = new FileStream(device.EvdevPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            open = true;
        }

        public void Read()
        {
            byte[] buffer = new byte[24];
            try
            {
                while (open)
                {
                    stream.Read(buffer, 0, buffer.Length);

                    // TODO: parse timeval (16 bytes)
                    int offset = 16;
                    short type = BitConverter.ToInt16(new byte[] { buffer[offset], buffer[++offset] }, 0);
                    short code = BitConverter.ToInt16(new byte[] { buffer[++offset], buffer[++offset] }, 0);
                    int value = BitConverter.ToInt32(
                        new byte[] { buffer[++offset], buffer[++offset], buffer[++offset], buffer[++offset] }, 0);

                    // Console.WriteLine("Type={0}, Code={1}, Value={2}", type, code, value);

                    // Dispatch corresponding gamepad event to any subscribed event handlers
                    DispatchEvent(type, code, value);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        public void Stop()
        {
            open = false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            
            CloseStream();
            disposed = true;
        }

        ~EvdevReader()
        {
            Dispose(false);
        }

        private void CloseStream()
        {
            Stop();

            if (stream != null)
            {
                stream.Close();
                stream.Dispose();
                stream = null;
            }
        }

        private void DispatchEvent(short type, short code, int value)
        {
            // We only want to deal with gamepad events for now
            if (device.IsGamepad && device is GenericGamepad)
            {
                GenericGamepad gamepad = device as GenericGamepad;
                GamepadEventArgs.EventType eventType = (GamepadEventArgs.EventType)type;
                switch (eventType)
                {
                    case GamepadEventArgs.EventType.Syn:

                        break;

                    case GamepadEventArgs.EventType.Abs:
                        gamepad.DispatchAbsEvent(code, value);
                        break;

                    case GamepadEventArgs.EventType.Key:
                        // key down and key up events
                        gamepad.DispatchKeyEvent(code, value);
                        break;

                    case GamepadEventArgs.EventType.Msc:

                        break;
                }
            }
        }
    }
}
