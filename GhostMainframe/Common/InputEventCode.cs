﻿/*
 * Copyright (c) 2017 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace GhostMainframe.Common
{
    /// <summary>
    /// The Linux input event codes corresponding to uapi/linux/input-event-codes.h.
    /// </summary>
    public enum InputEventCode
    {
        KeyBack     = 0x09e,    // 158
        KeyHomepage = 0x0ac,    // 172

        // Gamepad event codes
        BtnGamepad  = 0x130,    // 304
        BtnSouth    = 0x130,    // 304
        BtnA        = BtnSouth,
        BtnEast     = 0x131,    // 305
        BtnB        = BtnEast,
        BtnC        = 0x132,    // 306
        BtnNorth    = 0x133,    // 307
        BtnX        = BtnNorth,
        BtnWest     = 0x134,    // 308
        BtnY        = BtnWest,
        BtnZ        = 0x135,    // 309
        BtnTL       = 0x136,    // 310
        BtnTR       = 0x137,    // 311
        BtnTL2      = 0x138,    // 312
        BtnTR2      = 0x139,    // 313
        BtnSelect   = 0x13a,    // 314
        BtnStart    = 0x13b,    // 315
        BtnMode     = 0x13c,    // 316
        BtnThumbL   = 0x13d,    // 317
        BtnThumbR   = 0x13e     // 318
    }
}
