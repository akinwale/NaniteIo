/*
 * Copyright (c) 2016 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Threading;
using Nanite.IO;
using Nanite.IO.Devices;
using Nanite.Exceptions;

namespace NaniteIoSample
{
    class Program
    {
        public static void Main (string[] args)
        {
            // Interactive Mode
            /*string input;
            do
            {
                Console.Write("Enter PIN number: ");
                input = Console.ReadLine();

                int pin = int.Parse(input);
                GPIO.PinMode(pin, GPIO.Direction.Output);

                GPIO.Write(pin, GPIO.Value.High);
                Thread.Sleep(200);
                GPIO.Write(pin, GPIO.Value.Low);

                GPIO.ClosePin(pin);
            } while (input != "-1");

            Console.WriteLine("End of program reached.");
            Console.ReadLine();*/

            /*int pin = (int) PINE64.Pi.Pin12;
            // Simple GPIO
            GPIO.PinMode(pin, GPIO.Direction.Output);

            // This will blink a LED connected to the pin 50 times
            for (int i = 0; i < 50; i++)
            {
                GPIO.Write(pin, GPIO.Value.High);
                Thread.Sleep(200);

                GPIO.Write(pin, GPIO.Value.Low);
                Thread.Sleep(200);
            }

            // Close the pin after we're done
            GPIO.ClosePin(pin);*/

            // Extended IO via I2C
            using (I2CExtendedIO eio = new I2CExtendedIO())
            {
                try
                {
                    eio.Open(0x08);

                    /*eio.AnalogWrite(8, 255);
                    eio.AnalogWrite(9, 255);
                    eio.DigitalWrite(4, GPIO.Value.High);
                    eio.DigitalWrite(6, GPIO.Value.High);
                    Console.WriteLine("Writing...");

                    Thread.Sleep(2000);

                    eio.DigitalWrite(4, GPIO.Value.Low);
                    eio.DigitalWrite(6, GPIO.Value.Low);

                    Console.WriteLine("Done");*/

                    string input;
                    do
                    {
                        Console.Write("Enter PIN and value: ");
                        input = Console.ReadLine();

                        if (input == "-1")
                        {
                            break;
                        }

                        string[] inputParts = input.Split(new char[] { ' ' });
                        if (inputParts.Length != 2 && inputParts.Length != 3)
                        {
                            Console.WriteLine("Invalid PIN and value. Try again.");
                            continue;
                        }

                        try
                        {
                            int pin = int.Parse(inputParts[0].Trim());
                            string value = inputParts[1];
                            if (value != "off" && value != "on" && inputParts.Length == 2)
                            {
                                Console.WriteLine("Invalid PIN and value. Try again.");
                                continue;
                            }

                            if (inputParts.Length == 3)
                            {
                                int analogValue = int.Parse(inputParts[2].Trim());
                                if (value != "analog")
                                {
                                    Console.WriteLine("Invalid PIN and value. Try again.");
                                    continue;
                                }

                                eio.AnalogWrite(pin, analogValue);
                            }
                            else
                            {
                                eio.DigitalWrite(pin, "on" == value ? GPIO.Value.High : GPIO.Value.Low);
                            }
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Invalid PIN and value. Try again.");
                            continue;
                        }
                    } while (input != "-1");
                }
                catch (ExtendedIOException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }


            Console.ReadLine();
        }
    }
}
